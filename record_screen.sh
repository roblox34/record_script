#!/usr/bin/env sh

set -euo pipefail

###########################
# config section          #
###########################

## rofi settings
# seperate with a newline 
default_save_dirs="$HOME"

## commands
recorder="wf-recorder"
snapper="grim"
part_select="slurp"
clipboard_manager="wl-copy"

## rofi config
rofi_flags="-dmenu"
rofi_theme_options="-fixed-num-lines false -auto-select true"

## recorder config
valid_extensions=$(ffmpeg -muxers 2>/dev/null | tail -n +5 | cut -d " " -f 4 | tr '\n' ' ' | sed 's/ *$//g')

## text lines
rec_start="Start recording"
rec_stop="Stop recording"
screenshot="Take a screenshot"
record_part="Part of the screen"
record_whole="The whole screen"
prompt_save="Save as >"
prompt_choose="Choose one > "

###########################

# check if a recording is running atm
if [ -z "$(pgrep ${recorder})" ]; then
    option_rec=$rec_start
else 
    option_rec=$rec_stop
fi

# prompt what to do
selection=$(echo "$option_rec\n$screenshot" | rofi $rofi_flags $rofi_theme_options -p $prompt_choose)

# stop the current recording
if [[ $selection == $rec_stop ]]; then
    pkill -2 $recorder
    rofi -e "Finished recording"
    exit
fi

# otherwise, ask wether to record/snap the whole screen or just a part
area_choice=$(echo "$record_whole\n$record_part" | rofi $rofi_flags $rofi_theme_options -p $prompt_choose)

case $selection in
    $rec_start)
        # start recording
        # prompt where to save
        outfile=$(echo "$default_save_dirs" | rofi $rofi_flags $rofi_theme_options -p $prompt_save);

        # if no extension or an invalid one is provided, set it to mp4
        # in case of an existing directory save using the current date/time
        if [ -z "$outfile" ]; then
            exit
        elif [ -d "$outfile" ]; then
            cur=$(date "+%Y%m%d_%H%M")
            outfile=$outfile"/${cur}.mp4"
        elif [ -z "${outfile##*.}" ]; then
            outfile="$outfile.mp4"
        elif [[ ! $valid_extensions =~ (^|[[:space:]])"${outfile##*.}"($|[[:space:]]) ]]; then
            rofi -e "Invalid extension!"
            exit
        fi

        # finally, record it all
        if [[ "$area_choice" == "$record_whole" ]]; then
            rofi -e "Recording to $(realpath ${outfile})"
            $recorder -f $outfile >/dev/null 2>&1 &
        else 
            sel_area=$($part_select)
            rofi -e "Recording to $(realpath ${outfile})"
            $recorder -g "$sel_area" -f "$outfile" >/dev/null 2>&1 &
        fi
        ;;
    $screenshot)
        if [[ "$area_choice" == "$record_whole" ]]; then
            $snapper - | wl-copy
        else 
            sel_area=$($part_select)
            $snapper -g "$sel_area" - | wl-copy
        fi
        ;;
esac
